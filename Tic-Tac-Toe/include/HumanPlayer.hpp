#ifndef HUMAN_PLAYER_HPP
#define HUMAN_PLAYER_HPP

#include "Player.hpp"
#include <iostream>

class HumanPlayer final : public Player
{
private:
    bool checkIfMovePossible(const std::vector<Marks> &board, const unsigned int size, Move move);

public:
    HumanPlayer(std::string newName, Marks newMark) : Player{newName, newMark} {};
    ~HumanPlayer() = default;
    void makeMove(std::vector<Marks> &board, const unsigned int size) override final;
};

#endif