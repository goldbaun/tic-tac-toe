#ifndef UTILITY_HPP
#define UTILITY_HPP

/**
 * @brief Stores move's informations
 */
struct Move
{
    Move() = default;
    Move(unsigned int r, unsigned int c) : row(r), column(c){};
    unsigned int row;
    unsigned int column;
};

/**
 * @brief Possible marks on game board
 */
enum class Marks : char
{
    X = 'x',
    O = 'o',
    NONE = '-'
};

/**
 * @brief Indicates whose turn is now
 */
enum class Turns : bool
{
    PLAYER1 = true,
    PLAYER2 = false
};

/**
 * @brief Stores possible states of game
 */
enum class StatesOfGame : int
{
    WON = 1,
    DRAW = 2,
    CONTINUE = 3
};

#endif
