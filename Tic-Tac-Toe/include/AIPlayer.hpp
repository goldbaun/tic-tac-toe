#ifndef AI_PLAYER_HPP
#define AI_PLAYER_HPP

#include "Player.hpp"
#include <utility>
#include <algorithm>

// Special moves
#define WINNING_MOVE 1000
#define LOSING_MOVE -1000
#define CENTRE 100
#define ADJACENT 100
#define NORMAL 0

#define START_DEPTH 0
#define MAX_DEPTH 6
#define WIN 10
#define LOSE -10
#define TIE 0

class AIPlayer final : public Player
{
private:
    std::pair<bool, Move> inOne(std::vector<Marks> &board, const unsigned int size);
    const int evaluateMove(Move move, std::vector<Marks> &board, const unsigned int size, Marks marker);
    const int evaluateBoard(std::vector<Marks> &board, const unsigned int size);
    std::vector<Move> getLegalMoves(std::vector<Marks> &board, const unsigned int size);
    bool boardIsFull(std::vector<Marks> &board, const unsigned int size);
    std::pair<int, Move> minimax(std::vector<Marks> &board, const unsigned int size, Marks marker, unsigned int depth, int alpha, int beta);

public:
    AIPlayer(std::string newName, Marks newMark) : Player{newName, newMark} {};
    ~AIPlayer() = default;
    void makeMove(std::vector<Marks> &board, const unsigned int size) override final;
};

#endif