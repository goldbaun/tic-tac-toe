#ifndef GAME_HPP
#define GAME_HPP

#include "AIPlayer.hpp"
#include "HumanPlayer.hpp"
#include <memory>
#include <vector>
#include <iostream>

class Game
{
private:
    std::unique_ptr<Player> player1;
    std::unique_ptr<Player> player2;
    Turns turn;
    std::unique_ptr<std::vector<Marks>> board;
    unsigned int sizeOfBoard;

public:
    void Initialize(unsigned int choice, unsigned int size, std::string nick1, std::string nick2);
    void Begin();
    void drawBoard();
    bool isMovesLeft();
    std::vector<Marks> &stateOfBoard();
    StatesOfGame checkForWinner();
    void announceResult(StatesOfGame end);
};

#endif