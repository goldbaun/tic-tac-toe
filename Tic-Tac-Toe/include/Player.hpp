#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <string>
#include <vector>
#include "Utility.hpp"

class Player
{
protected:
    std::string name;
    Marks mark;

public:
    Player(std::string newName, Marks newMark);
    const std::string getName();
    const Marks getMark();
    virtual void makeMove(std::vector<Marks> &board, const unsigned int size) = 0;
};

#endif