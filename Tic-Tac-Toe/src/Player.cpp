#include "../include/Player.hpp"

Player::Player(std::string newName, Marks newMark) : name(newName), mark(newMark){};

/**
 * @brief Return player's nick
 * @return player's nick
 */
const std::string Player::getName()
{
    return name;
}

/**
 * @brief Return player's mark
 * @return player's mark
 */
const Marks Player::getMark()
{
    return mark;
}
