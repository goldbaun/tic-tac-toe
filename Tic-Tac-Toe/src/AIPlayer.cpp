#include "../include/AIPlayer.hpp"

/**
 * @brief Check if on current state of board someone won, lost or it is tie
 * @param board reference to gameboard
 * @param size size of board
 * @return WIN if player won, LOSE if he lost or TIE when it is draw
 */
const int AIPlayer::evaluateBoard(std::vector<Marks> &board, const unsigned int size)
{
    unsigned int playersMarks = 0;
    unsigned int opponentsMarks = 0;

    // Checking rows for win
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (board.at(j + (i * size)) == mark)
                playersMarks++;
            else if (board.at(j + (i * size)) != Marks::NONE && board.at(j + (i * size)) != mark)
                opponentsMarks++;
        }
        if ((playersMarks) == size)
            return WIN;
        else if ((opponentsMarks) == size)
            return LOSE;
        playersMarks = 0;
        opponentsMarks = 0;
    }

    // Checking columns for win
    playersMarks = 0;
    opponentsMarks = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (board.at(i + (j * size)) == mark)
                playersMarks++;
            else if (board.at(i + (j * size)) != Marks::NONE && board.at(i + (j * size)) != mark)
                opponentsMarks++;
        }
        if ((playersMarks) == size)
            return WIN;
        else if ((opponentsMarks) == size)
            return LOSE;
        playersMarks = 0;
        opponentsMarks = 0;
    }

    // Checking diagonals for win
    playersMarks = 0;
    opponentsMarks = 0;
    for (int i = 0; i < size * size; i += size + 1)
    {
        if (board.at(i) == mark)
            playersMarks++;
        else if (board.at(i) != Marks::NONE && board.at(i) != mark)
            opponentsMarks++;
    }
    if ((playersMarks) == size)
        return WIN;
    else if ((opponentsMarks) == size)
        return LOSE;

    playersMarks = 0;
    opponentsMarks = 0;
    for (int i = size - 1; i < ((size * size) - (size - 1)); i += (size - 1))
    {
        if (board.at(i) == mark)
            playersMarks++;
        else if (board.at(i) != Marks::NONE && board.at(i) != mark)
            opponentsMarks++;
    }
    if ((playersMarks) == size)
        return WIN;
    else if ((opponentsMarks) == size)
        return LOSE;

    // If nothing wins
    return TIE;
}

/**
 * @brief Looks for all legal moves on current state of board
 * @param board reference to gameboard
 * @param size size of board
 * @return vector of legal moves
 */
std::vector<Move> AIPlayer::getLegalMoves(std::vector<Marks> &board, const unsigned int size)
{
    std::vector<Move> legalMoves;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (board.at(j + (i * size)) == Marks::NONE)
                legalMoves.push_back(Move(i, j));
        }
    }
    return legalMoves;
}

/**
 * @brief Check if board is full
 * @param board reference to gameboard
 * @param size size of board
 * @return true if full, false if not
 */
bool AIPlayer::boardIsFull(std::vector<Marks> &board, const unsigned int size)
{
    std::vector<Move> legalMoves = getLegalMoves(board, size);
    if (0 == legalMoves.size())
        return true;
    else
        return false;
}

/**
 * @brief Min-max algorithm with alpha-beta pruning
 * @param board reference to gameboard
 * @param size size of board
 * @param marker indicates whose turn it is
 * @param depth depth of recursion
 * @param alpha value of alpha
 * @param beta value of beta
 * @return pair of value (the best score and the best move so far)
 */
std::pair<int, Move> AIPlayer::minimax(std::vector<Marks> &board, const unsigned int size, Marks marker, unsigned int depth, int alpha, int beta)
{
    // Initialize best move
    Move bestMove(size, size);
    Move currentMove;
    int bestScore = (marker == mark) ? LOSE : WIN;
    Marks opponnetMark = (mark == Marks::X) ? Marks::O : Marks::X;

    // If we hit a final state, return the best score and move
    if (boardIsFull(board, size) || TIE != evaluateBoard(board, size))
    {
        bestScore = evaluateBoard(board, size);
        return std::make_pair(bestScore, bestMove);
    }

    if (depth == MAX_DEPTH)
    {
        bestScore = evaluateBoard(board, size);
        return std::make_pair(bestScore, bestMove);
    }

    std::vector<Move> legalMoves = getLegalMoves(board, size);

    for (const auto i : legalMoves)
    {
        currentMove = i;
        board.at(currentMove.column + (currentMove.row * size)) = marker;

        // Maximizing player's turn
        if (marker == mark)
        {
            int score = minimax(board, size, opponnetMark, depth + 1, alpha, beta).first;
            score += evaluateMove(currentMove, board, size, mark);

            if (bestScore < score)
            {
                bestScore = score - depth * 10;
                bestMove = currentMove;

                alpha = std::max(alpha, bestScore);
                board.at(currentMove.column + (currentMove.row * size)) = Marks::NONE;
                if (beta <= alpha)
                    break;
            }

        } // Minimizing opponent's turn
        else
        {
            int score = minimax(board, size, mark, depth + 1, alpha, beta).first;
            score += evaluateMove(currentMove, board, size, opponnetMark);

            if (bestScore > score)
            {
                bestScore = score + depth * 10;
                bestMove = currentMove;

                beta = std::min(beta, bestScore);
                board.at(currentMove.column + (currentMove.row * size)) = Marks::NONE;
                if (beta <= alpha)
                    break;
            }
        }

        board.at(currentMove.column + (currentMove.row * size)) = Marks::NONE;
    }

    return std::make_pair(bestScore, bestMove);
}

/**
 * @brief Makes the best move in actual position
 * @param board reference to gameboard
 * @param size size of board
 */
void AIPlayer::makeMove(std::vector<Marks> &board, const unsigned int size)
{
    // Check if possible to end in one move
    std::pair<bool, Move> inOneMove = inOne(board, size);
    if (inOneMove.first)
        board.at(inOneMove.second.column + (inOneMove.second.row * size)) = mark;
    
    // Make best move
    else
    {
        std::pair<int, Move> computerMove = minimax(board, size, mark, START_DEPTH, LOSE, WIN);
        board.at(computerMove.second.column + (computerMove.second.row * size)) = mark;
    }
}

/**
 * @brief Calculate additional score for move
 * @param move move which score is calculated
 * @param board reference to gameboard
 * @param size size of board
 * @param marker mark of player whose move is calculated
 * @return calculated score
 */
const int AIPlayer::evaluateMove(Move move, std::vector<Marks> &board, const unsigned int size, Marks marker)
{
    int extraScore(NORMAL);

    // instant win
    if (evaluateBoard(board, size) == WIN && marker == mark)
    {
        extraScore += WINNING_MOVE;
        return extraScore;
    }

    // instant lose
    if (evaluateBoard(board, size) == LOSE && marker != mark)
    {
        extraScore += LOSING_MOVE;
        return extraScore;
    }

    // additional score for adjacent squares
    if (move.row == ((size - 1) / 2) && move.column == ((size - 1) / 2))
        extraScore += CENTRE;

    if (move.row > 0 && board.at(move.column + (move.row - 1) * size) == marker)
        extraScore += ADJACENT;
    if (move.row < size - 1 && board.at(move.column + (move.row + 1) * size) == marker)
        extraScore += ADJACENT;

    if (move.column > 0 && board.at(move.row + (move.column - 1) * size) == marker)
        extraScore += ADJACENT;
    if (move.column < size - 1 && board.at(move.row + (move.column + 1) * size) == marker)
        extraScore += ADJACENT;

    return extraScore;
}

/**
 * @brief Check if game can be ended in one move
 * @param board reference to gameboard
 * @param size size of board
 * @return pair of value (true/false and move)
 */
std::pair<bool, Move> AIPlayer::inOne(std::vector<Marks> &board, const unsigned int size)
{
    // Looking for lose in one
    Move currentMove;
    Marks opponnetMark = (mark == Marks::X) ? Marks::O : Marks::X;
    std::vector<Move> legalMoves = getLegalMoves(board, size);
    for (const auto i : legalMoves)
    {
        currentMove = i;
        board.at(currentMove.column + (currentMove.row * size)) = opponnetMark;
        int score = evaluateMove(currentMove, board, size, opponnetMark);
        if (score == LOSING_MOVE)
        {
            board.at(currentMove.column + (currentMove.row * size)) = Marks::NONE;
            return std::make_pair(true, currentMove);
        }
        board.at(currentMove.column + (currentMove.row * size)) = Marks::NONE;
    }

    // Looking for win in one

    for (const auto i : legalMoves)
    {
        currentMove = i;
        board.at(currentMove.column + (currentMove.row * size)) = mark;
        int score = evaluateMove(currentMove, board, size, mark);
        if (score == WINNING_MOVE)
        {
            board.at(currentMove.column + (currentMove.row * size)) = Marks::NONE;
            return std::make_pair(true, currentMove);
        }
        if (score == CENTRE)
        {
            board.at(currentMove.column + (currentMove.row * size)) = Marks::NONE;
            return std::make_pair(true, currentMove);
        }
        board.at(currentMove.column + (currentMove.row * size)) = Marks::NONE;
    }

    return std::make_pair(false, currentMove);
}
