#include "../include/AIPlayer.hpp"
#include "../include/HumanPlayer.hpp"
#include "../include/Game.hpp"
#include <iostream>
#include <limits>

#define GAME_MODE_1 1
#define GAME_MODE_2 2
#define GAME_MODE_3 3
#define MIN_BOARD 3
#define MAX_BOARD 7

int main(void)
{
    Game mainGame;
    int choice(0);
    bool endOfInit(false);
    int sizeOfBoard(0);
    char end;
    std::string nick1;
    std::string nick2;
    while (!endOfInit)
    {
        std::cout << "Wybierz tryb gry:" << std::endl;
        std::cout << "1 - Gracz vs Komputer" << std::endl;
        std::cout << "2 - Gracz vs Gracz" << std::endl;
        std::cout << "3 - Komputer vs Komputer" << std::endl;
        std::cin >> choice;
        if (std::cin.fail())
        {
            std::cout << "Wybrano bledny tryb" << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else if (choice < 1 || choice > 3)
        {
            std::cout << "Wybrano bledny tryb" << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else if (choice == GAME_MODE_1)
        {
            std::cout << "Podaj nick gracza 1:" << std::endl;
            std::cin >> nick1;
            nick2 = "COMPUTER";
            endOfInit = true;
        }
        else if (choice == GAME_MODE_2)
        {
            std::cout << "Podaj nick gracza 1:" << std::endl;
            std::cin >> nick1;
            std::cout << std::endl;
            std::cout << "Podaj nick gracza 2:" << std::endl;
            std::cin >> nick2;
            endOfInit = true;
        }
        else if (choice == GAME_MODE_3)
        {
            nick1 = "COMPUTER 1";
            nick2 = "COMPUTER 2";
            endOfInit = true;
        }
    }

    endOfInit = false;
    while (!endOfInit)
    {
        std::cout << "Wybierz rozmiar planszy (3-7):" << std::endl;
        std::cin >> sizeOfBoard;
        if (std::cin.fail())
        {
            std::cout << "Wybrano bledny rozmiar" << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else if (sizeOfBoard < MIN_BOARD || sizeOfBoard > MAX_BOARD)
        {
            std::cout << "Wybrano bledny rozmiar" << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else
            endOfInit = true;
    }
    mainGame.Initialize(choice, sizeOfBoard, nick1, nick2);
    mainGame.Begin();

std::cin >> end;
}
