#include "../include/Game.hpp"

/**
 * @brief Initialize game with given options
 * @param choice game mode
 * @param size size of board
 * @param nick1 nick of player 1
 * @param nick2 nick of player 2
 */
void Game::Initialize(unsigned int choice, unsigned int size, std::string nick1, std::string nick2)
{
    switch (choice)
    {
    case 1:
        player1 = std::make_unique<HumanPlayer>(nick1, Marks::X);
        player2 = std::make_unique<AIPlayer>(nick2, Marks::O);
        break;
    case 2:
        player1 = std::make_unique<HumanPlayer>(nick1, Marks::X);
        player2 = std::make_unique<HumanPlayer>(nick2, Marks::O);
        break;
    case 3:
        player1 = std::make_unique<AIPlayer>(nick1, Marks::X);
        player2 = std::make_unique<AIPlayer>(nick2, Marks::O);
        break;
    default:
        break;
    }
    board = std::make_unique<std::vector<Marks>>(size * size, Marks::NONE);
    sizeOfBoard = size;
    turn = Turns::PLAYER1;
}

/**
 * @brief Begin the game
 */
void Game::Begin()
{
    bool gameEnd(false);
    StatesOfGame stateOfGame(StatesOfGame::CONTINUE);
    drawBoard();
    // Main loop of game
    while (!gameEnd)
    {
        if (turn == Turns::PLAYER1)
        {
            player1->makeMove(stateOfBoard(), sizeOfBoard);
            turn = Turns::PLAYER2;
        }
        else
        {
            player2->makeMove(stateOfBoard(), sizeOfBoard);
            turn = Turns::PLAYER1;
        }
        drawBoard();
        stateOfGame = checkForWinner();
        if (stateOfGame != StatesOfGame::CONTINUE)
        {
            announceResult(stateOfGame);
            gameEnd = true;
        }
    }
}

/**
 * @brief Check if any moves left 
 * @return true if any moves left, false if  not
 */
bool Game::isMovesLeft()
{
    for (int i = 0; i < sizeOfBoard * sizeOfBoard; i++)
    {
        if (board->at(i) == Marks::NONE)
            return true;
    }
    return false;
}

/**
 * @brief Announce result of game
 * @param result result of game
 */
void Game::announceResult(StatesOfGame result)
{
    if (result == StatesOfGame::WON)
    {
        if (turn == Turns::PLAYER1)
        {
            std::cout << "Wygrywa gracz 2: " << player2->getName() << std::endl;
        }
        else
            std::cout << "Wygrywa gracz 1: " << player1->getName() << std::endl;
    }
    else
        std::cout << "REMIS" << std::endl;
}

/**
 * @brief Draws a current game board
 */
void Game::drawBoard()
{
    for (int i = 0; i < sizeOfBoard; i++)
    {
        for (int j = 0; j < sizeOfBoard; j++)
        {
            if (j == (sizeOfBoard - 1))
                std::cout << (char)(board->at(j + (i * sizeOfBoard)));
            else
                std::cout << (char)(board->at(j + (i * sizeOfBoard))) << '|';
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

/**
 * @brief Return reference to game board
 * @return reference to game board
 */
std::vector<Marks> &Game::stateOfBoard()
{
    return *board.get();
}

/**
 * @brief Check if someone won
 * @return WON, DRAW or CONTINUE
 */
StatesOfGame Game::checkForWinner()
{
    unsigned int playerMarks = 0;

    // Checking rows for win
    for (int i = 0; i < sizeOfBoard; i++)
    {
        for (int j = 0; j < sizeOfBoard; j++)
        {
            if (board->at(j + (i * sizeOfBoard)) == player1->getMark() && turn == Turns::PLAYER2)
                playerMarks++;
            else if (board->at(j + (i * sizeOfBoard)) == player2->getMark() && turn == Turns::PLAYER1)
                playerMarks++;
        }
        if (playerMarks == sizeOfBoard)
            return StatesOfGame::WON;
        playerMarks = 0;
    }

    // Checking columns for win
    playerMarks = 0;
    for (int i = 0; i < sizeOfBoard; i++)
    {
        for (int j = 0; j < sizeOfBoard; j++)
        {
            if (board->at(i + (j * sizeOfBoard)) == player1->getMark() && turn == Turns::PLAYER2)
                playerMarks++;
            else if (board->at(i + (j * sizeOfBoard)) == player2->getMark() && turn == Turns::PLAYER1)
                playerMarks++;
        }
        if (playerMarks == sizeOfBoard)
            return StatesOfGame::WON;
        playerMarks = 0;
    }

    // Checking diagonals for win
    playerMarks = 0;
    for (int i = 0; i < (sizeOfBoard * sizeOfBoard); i += sizeOfBoard + 1)
    {
        if (board->at(i) == player1->getMark() && turn == Turns::PLAYER2)
            playerMarks++;
        else if (board->at(i) == player2->getMark() && turn == Turns::PLAYER1)
            playerMarks++;
    }
    if (playerMarks == sizeOfBoard)
        return StatesOfGame::WON;

    playerMarks = 0;
    for (int i = sizeOfBoard - 1; i < ((sizeOfBoard * sizeOfBoard) - (sizeOfBoard - 1)); i += (sizeOfBoard - 1))
    {
        if (board->at(i) == player1->getMark() && turn == Turns::PLAYER2)
            playerMarks++;
        else if (board->at(i) == player2->getMark() && turn == Turns::PLAYER1)
            playerMarks++;
    }
    if (playerMarks == sizeOfBoard)
        return StatesOfGame::WON;

    // Checking for draw
    for (int i = 0; i < sizeOfBoard; i++)
    {
        for (int j = 0; j < sizeOfBoard; j++)
        {
            if (board->at(j + (i * sizeOfBoard)) == Marks::NONE)
                return StatesOfGame::CONTINUE;
        }
    }
    return StatesOfGame::DRAW;
}