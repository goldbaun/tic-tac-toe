#include "../include/HumanPlayer.hpp"

/**
 * @brief Check if move is legal
 * @param board reference to gameboard
 * @param size size of board
 * @param move given move
 * @return true if legal, false if not
 */
bool HumanPlayer::checkIfMovePossible(const std::vector<Marks> &board, const unsigned int size, Move move)
{
    return board.at(move.column + (move.row * size)) == Marks::NONE ? true : false;
}

/**
 * @brief Ask user what move he want to do
 * @param board reference to gameboard
 * @param size size of board
 */
void HumanPlayer::makeMove(std::vector<Marks> &board, const unsigned int size)
{
    Move newMove;
    bool moveMade(false);
    bool errorFlag(false);
    while (!moveMade)
    {
        std::cout << "Ktore pole chcesz zajac (Najpierw podaj wiersz, pozniej kolumne)?: " << std::endl;
        std::cin >> newMove.row;
        if (std::cin.fail())
        {
            std::cout << "BLAD! Podaj ponownie" << std::endl;
            errorFlag = true;
            std::cin.clear();
        }
        else
            std::cin >> newMove.column;
        if (std::cin.fail() || errorFlag)
        {
            std::cout << "BLAD! Podaj ponownie" << std::endl;
            std::cin.clear();
            errorFlag = false;
        }
        else if (checkIfMovePossible(board, size, newMove))
        {
            board.at(newMove.column + (newMove.row * size)) = mark;
            moveMade = true;
        }
        else
            std::cout << "Pole zajete! Podaj pole ponownie" << std::endl;
    }
}