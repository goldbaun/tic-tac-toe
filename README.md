# Tic-Tac-Toe

Console version of tic-tac-toe game. Game is using min-max algorithm wit alpha-beta pruning.\
You can choose gamemode and size of board.

1. Game modes:\
![alt text](https://i.imgur.com/bpZ0ZL2.png)

2. Gameplay:\
![alt text](https://i.imgur.com/5F2i2SA.png)
